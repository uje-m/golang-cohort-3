package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter your texts:")
	words, _ := reader.ReadString('\n')

	words = strings.TrimRight(words, "\n")
	wordMap := map[string]int{}

	fmt.Println("You entered:", words)

	for _, char := range words {
		fmt.Printf("%c\n", char)
		wordMap[string(char)] = strings.Count(words, string(char))
	}
	fmt.Println(wordMap)

}
