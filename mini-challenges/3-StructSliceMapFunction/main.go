package main

import (
	"fmt"
	"os"
	"strconv"
)

type student struct {
	ID        int
	Nama      string
	Alamat    string
	Pekerjaan string
	Alasan    string
}

var students = []student{
	{ID: 1, Nama: "John", Alamat: "123 Main St", Pekerjaan: "Engineer", Alasan: "Interest in programming"},
	{ID: 2, Nama: "Jane", Alamat: "456 Elm St", Pekerjaan: "Teacher", Alasan: "Career change"},
	{ID: 3, Nama: "Michael", Alamat: "789 Oak St", Pekerjaan: "Doctor", Alasan: "Family tradition"},
	{ID: 4, Nama: "Emily", Alamat: "101 Pine St", Pekerjaan: "Artist", Alasan: "Passion for art"},
	{ID: 5, Nama: "William", Alamat: "202 Cedar St", Pekerjaan: "Lawyer", Alasan: "Desire to help others"},
	{ID: 6, Nama: "Olivia", Alamat: "303 Maple St", Pekerjaan: "Chef", Alasan: "Love for cooking"},
	{ID: 7, Nama: "James", Alamat: "404 Birch St", Pekerjaan: "Athlete", Alasan: "Pursuit of excellence"},
	{ID: 8, Nama: "Sophia", Alamat: "505 Walnut St", Pekerjaan: "Scientist", Alasan: "Curiosity about the world"},
	{ID: 9, Nama: "Benjamin", Alamat: "606 Ash St", Pekerjaan: "Musician", Alasan: "Passion for music"},
	{ID: 10, Nama: "Isabella", Alamat: "707 Spruce St", Pekerjaan: "Entrepreneur", Alasan: "Desire for independence"},
}

func findStudent(arg string) {
	found := false

	for _, student := range students {
		if strconv.Itoa(student.ID) == arg || student.Nama == arg {
			fmt.Printf("ID : %d\nnama : %s\nalamat : %s\npekerjaan : %s\nalasan : %s\n",
				student.ID, student.Nama, student.Alamat, student.Pekerjaan, student.Alasan)
			found = true
			break
		}
	}

	if !found {
		fmt.Println("Data dengan nama/absen tsb tidak tersedia")
	}
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Tolong masukan nama atau nomor absen")
		fmt.Println("Contoh: 'go run main.go Fitri' atau 'go run main.go 2'")
		return
	}

	arg := os.Args[1]
	findStudent(arg)
}
