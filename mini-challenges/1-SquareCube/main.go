package main

import (
	"fmt"
	"math"
)

func main() {
	var number int
	fmt.Print("Enter you Max number for SquareCube: ")
	_, err := fmt.Scanln(&number)
	if err != nil {
		fmt.Println(" Error reading input", err)
		return
	}

	fmt.Println("You entered:", number)

	for i := 1; i <= number; i++ {
		isSquare := math.Mod(math.Sqrt(float64(i)), 1) == 0
		isCube := math.Mod(math.Cbrt(float64(i)), 1) == 0

		switch {
		case isSquare && isCube:
			fmt.Println("SquareCube")
		case isSquare:
			fmt.Println("Square")
		case isCube:
			fmt.Println("Cube")
		default:
			fmt.Println(i)
		}
	}

}
